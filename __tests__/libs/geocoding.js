import {extractLatLng} from '../../src/libs/geocoding'

const getCoordMock = ()=>{
  return {"coords": {"accuracy": 13.26200008392334, "altitude": 774.300048828125, "heading": 0, "latitude": -23.5372074, "longitude": -46.5984717, "speed": 0}, "mocked": false, "timestamp": 1650048294527}
}

describe('Geocoding', ()=>{
  it('Extract lat lng', ()=>{
    expect(extractLatLng(getCoordMock())).toMatchObject({lat:-23.5372074, lng:-46.5984717})
  })

  it('Extract from error', ()=>{
    expect(extractLatLng({
      "PERMISSION_DENIED": 1,
      "POSITION_UNAVAILABLE": 2,
      "TIMEOUT": 3,
      "code": 3,
      "message": "Location request timed out",
    })).toMatchObject({lat:undefined, lng:undefined})
  })
})