import {addPctSign, addTempUnitSign, fetchWeatherByLocation, getVisibilityText} from '../../src/libs/weather'

jest.mock('../../src/libs/weather', () => {
  const originalModule = jest.requireActual('../../src/libs/weather');
  return {
    __esModule: true,
    ...originalModule,
    fetchWeatherByLocation: jest.fn(({lat, lng})=>{
      if(lat===-1 || lng===-1){
        return Promise.reject({
          cod: 400,
          message: 'wrong latitude',
        })
      }
      return Promise.resolve(getWeatherMock())
    })
  };
});

const getWeatherMock = () => {
  return {
    'cod': 200,
    'main': {
      'temp': 19.64,
      'feels_like': 19.07,
      'temp_min': 18.36,
      'temp_max': 20.81,
      'pressure': 1017,
      'humidity': 54,
    },
    'visibility': 10000,
    'wind': {
      'speed': 4.12,
      'deg': 180,
    },
    'clouds': {
      'all': 20,
    },
  }
}

describe('Weather', () => {
  it('Success weather fetch', async () => {
    await expect(fetchWeatherByLocation({lat: 123, lng: 456})).resolves.toMatchObject(getWeatherMock())
  })
  it('Failed fetch with wrong location', async () => {
    await expect(fetchWeatherByLocation({lat: -1, lng: -1})).rejects.toMatchObject({
      cod: 400,
      message: 'wrong latitude',
    })
  })

  it('Celcius sign', () => {
    expect(addTempUnitSign(100)).toBe('100°')
  })
  it('Percentage sign', () => {
    expect(addPctSign(100)).toBe('100%')
  })

  it('Visbility in KM', () => {
    expect(getVisibilityText(10000)).toBe('10KM')
  })
  it('Visbility in metters', () => {
    expect(getVisibilityText(850)).toBe('850M')
  })
})