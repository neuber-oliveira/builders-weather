import Config from "react-native-config";

const KM_METTER_SIZE=1000
export const getVisibilityText = (visibility)=>{
  let unit = 'KM'
  let vis = visibility

  if(visibility<KM_METTER_SIZE){
    unit = 'M'
  }else{
    vis = visibility/KM_METTER_SIZE
  }

  return vis+unit
}

export const addPctSign = (value)=>value+'%'
export const addTempUnitSign = (value)=>value+'°'

// https://openweathermap.org/current
export const fetchWeatherByLocation = async (location) => {
  const {lat, lng} = location;
  const url = `https://api.openweathermap.org/data/2.5/weather?units=metric&lang=pt_br&lat=${lat}&lon=${lng}&appid=${Config.OPEN_WEATHER_API}`;
  const result = await fetch(url).then(r=>r.json())
  const responseCode = result.cod

  // console.log('weather', result)
  const outOfSuccessCode = !(responseCode>=200 && responseCode<=299)
  if( outOfSuccessCode ){
    return Promise.reject(result)
  }
  return result
};

export const formatWeatherData = (weatherData)=>{
  const {main, visibility, wind, clouds} = weatherData

  return {
    main:{
      temp: addTempUnitSign(main.temp),
      feels_like: addTempUnitSign(main.feels_like),
      temp_min: addTempUnitSign(main.temp_min),
      temp_max: addTempUnitSign(main.temp_max),
      humidity: addPctSign(main.humidity),
    },
    visibility: getVisibilityText(visibility),
    wind: {speed: wind.speed+'KM', deg: wind.deg},
    clouds: {all: addPctSign(clouds.all)}
  }
}
