import Geolocation from 'react-native-geolocation-service';
import Config from "react-native-config";

export const fetchGeocodeAddress = async (location) => {
  const {lat, lng} = location;
  const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${Config.GEOCODE_API}`;
  const results = await fetch(url).then(r=>r.json())
  let addr = ''

  if(results.status!=='OK'){
    return Promise.reject(results)
  }

  // console.log('geocode', results.results[0].formatted_address)
  addr = results.results[0].formatted_address
  return addr
};

export const extractLatLng = (locationResult)=>({lat:locationResult?.coords?.latitude, lng: locationResult?.coords?.longitude,})
export const getCurrentLocation = async () => {
  // info {"coords": {"accuracy": 13.26200008392334, "altitude": 774.300048828125, "heading": 0, "latitude": -23.5372074, "longitude": -46.5984717, "speed": 0}, "mocked": false, "timestamp": 1650048294527}
  return new Promise((resolve, reject) => {
      Geolocation.getCurrentPosition(
        info => {
          resolve(info);
        },
        err => reject(err),
        {enableHighAccuracy: false}
      );
  });
};
