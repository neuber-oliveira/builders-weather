import {View} from 'react-native'
import React, {useEffect, useState} from 'react'
import {extractLatLng, fetchGeocodeAddress, getCurrentLocation} from '../libs/geocoding'
import {fetchWeatherByLocation, formatWeatherData} from '../libs/weather'
import IconNavigation from '../icon/IconNavigation'
import {Box, Center, Flex, Text} from 'native-base'

const AuthorizedContent = () => {
  const [weather, setWeather] = useState();
  const [address, setAddress] = useState();
  // const [location, setLocation] = useState();

  const fetchAddress = async location => {
    const addr = await fetchGeocodeAddress(location);
    setAddress(addr);
  };

  const fetchWeather = async location => {
    const weatherData = await fetchWeatherByLocation(location);
    setWeather(formatWeatherData(weatherData));
  };

  const collectData = async () => {
    const locationResult = await getCurrentLocation();
    const latlng = extractLatLng(locationResult)

    await Promise.all([fetchAddress(latlng), fetchWeather(latlng)]);
    // setLocation(latlng);

  };

  useEffect(() => {
    collectData();
  }, []);

  return (
    <View>
      <Text>
        Que legal!!! Agora que eu tenho acesso a sua localização vou te trazer
        algumas informacoes sobre o clima aí onde você esta.
      </Text>

      <Box>
        <Box my="5">
          {address && (<Text textAlign="left">{address}</Text>)}
        </Box>

        <Box>
          {weather && (
            <Flex direction="row">
              <Flex flex={2} direction="column">
                <Center>
                  <Text fontSize="4xl">{weather.main.temp}</Text>
                </Center>

                <Flex direction="row" justify="space-between">
                  <Text>Realfeel</Text>
                  <Text>{weather.main.feels_like}</Text>
                </Flex>

                <Flex direction="row" justify="space-between" >
                  <Text>{weather.main.temp_min}</Text>
                  <Text>{weather.main.temp_max}</Text>
                </Flex>
              </Flex>

              <Flex flex={3} direction="column" pl="10">
                <Text>Humidade: {weather.main.humidity}</Text>
                <Text>Visibilidade: {weather.visibility}</Text>
                <Text>Nuvens: {weather.clouds.all}</Text>
                <Flex direction="row">
                  <Text>Vento: {weather.wind.speed}</Text>
                  <IconNavigation degrees={weather.wind.deg} />
                </Flex>
              </Flex>
            </Flex>
          )}
        </Box>
      </Box>
    </View>
  );
};

export default AuthorizedContent;
