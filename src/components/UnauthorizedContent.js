import React from 'react'
import PermissionInfo from '../components/permission/PermissionInfo'

const UnauthorizedContent = ({permissionStatus}) => {
  return <PermissionInfo permissionStatus={permissionStatus} />;
};

export default UnauthorizedContent;
