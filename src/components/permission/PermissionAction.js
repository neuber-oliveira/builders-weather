import React from 'react';
import {View} from 'react-native';
import PropTypes from 'prop-types';
import {Text, Button} from 'native-base'

const PermissionAction = ({text, buttonAction, buttonLabel}) => {
  return (
    <View>
      <Text mb="8">{text}</Text>
      {buttonLabel && <Button onPress={buttonAction}>{buttonLabel}</Button>}
    </View>
  );
};

PermissionAction.propTypes = {
  text: PropTypes.string.isRequired,
  buttonLabel: PropTypes.string,
  buttonAction: PropTypes.func,
};

export default PermissionAction;
