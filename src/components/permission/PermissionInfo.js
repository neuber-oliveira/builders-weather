import React from 'react';
import {View, Button, Text} from 'react-native';
import PropTypes from 'prop-types';
import GPSState from 'react-native-gps-state';
import PermissionAction from './PermissionAction';

const createPermissionInfoPayload = (text, label, action) => ({
  text,
  label,
  action,
});
const permisionMap = {
  [GPSState.NOT_DETERMINED]: createPermissionInfoPayload(
    'Para que este app funcione em toda a sua incrível capacidade, você precisa permitir o acesso a sua localizção. E não se preocupe, não vou perseguir você ;)',
    'EU PERMITO',
    () => GPSState.requestAuthorization(GPSState.AUTHORIZED_WHENINUSE),
  ),
  [GPSState.RESTRICTED]: createPermissionInfoPayload(
    'O GPS do seu aparelho parece estar desativado, liga ele para eu poder acessar a sua localizacao e te mostrar umas coisas bem legais',
    'ABRIR CONFIGURAÇÕES',
    GPSState.openLocationSettings,
  ),
  [GPSState.DENIED]: createPermissionInfoPayload(
    'Que pena que você não me permite acessar a sua localização, mas eu preciso mesmo disso para funcionar, por favor.',
    'OK, VAI LÁ',
    () => GPSState.requestAuthorization(GPSState.AUTHORIZED_WHENINUSE),
  ),
};

const PermissionInfo = ({permissionStatus}) => {
  const info = permisionMap[permissionStatus];

  if (!info) {
    return null;
  }

  return (
    <PermissionAction
      text={info.text}
      buttonLabel={info.label}
      buttonAction={info.action}
    />
  );
};

PermissionInfo.propTypes = {
  permissionStatus: PropTypes.number.isRequired,
};

export default PermissionInfo;
