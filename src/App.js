import React, {useEffect, useState} from 'react'
import {SafeAreaView, ScrollView, StatusBar, StyleSheet} from 'react-native'
import GPSState from 'react-native-gps-state'
import AuthorizedContent from './components/AuthorizedContent'
import UnauthorizedContent from './components/UnauthorizedContent'
import {NativeBaseProvider} from 'native-base/src/core/NativeBaseProvider'

const App = () => {
  const [gpsStatus, setGPSStatus] = useState(-1);

  const isAuthorized = GPSState.isAuthorized(gpsStatus);

  useEffect(() => {
    const unlinsten = GPSState.addListener(status => {
      setGPSStatus(status);
    });
    return unlinsten;
  }, []);

  return (
    <NativeBaseProvider>
      <SafeAreaView>
        <StatusBar/>
          <ScrollView style={styles.scrollview}  contentContainerStyle={styles.container} contentInsetAdjustmentBehavior="automatic" >
              {isAuthorized && <AuthorizedContent />}
              {!isAuthorized && <UnauthorizedContent permissionStatus={gpsStatus} />}
          </ScrollView>
      </SafeAreaView>
    </NativeBaseProvider>
  );
};

const styles = StyleSheet.create({
  container: {
  },
  scrollview: {
    padding: 8,
  },
});

export default App;
