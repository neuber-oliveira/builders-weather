import React from 'react'
import PropTypes from 'prop-types'
import {Image} from 'react-native'

const SIZE = 24
const IconNavigation = ({degrees}) => {
  const style = {
    width: SIZE,
    height: SIZE,
    // backgroundColor: '#F00',
    resizeMode: 'cover',
    transform: [{rotate: degrees + 'deg'}],
  }
  return (
    <Image style={style} source={require('../assets/icon/icon_navigation.png')}/>
  )
}


IconNavigation.propTypes = {
  degrees: PropTypes.number.isRequired,
}

IconNavigation.defaultProps = {
  degrees: 0,
}

export default IconNavigation
